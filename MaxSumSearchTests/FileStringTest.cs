using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxSumSearch;

namespace MaxSumSearchTests
{
    [TestClass]
    public class FileStringTest
    {
        [TestMethod]
        public void IsStringCorrectTest()
        {
            FileString fileString = new FileString("25.48,9.93,48,7.1", ",");

            Assert.IsTrue(fileString.IsStringCorrect);
        }

        [TestMethod]
        public void StringSumTest()
        {
            FileString fileString = new FileString("4.5,2.1,87.60", ",");

            Assert.AreEqual(fileString.StringSum, 94.199999999999989);
        }
    }
}
