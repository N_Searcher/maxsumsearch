﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxSumSearch;
using System;
using System.Collections.Generic;
using System.Text;

namespace MaxSumSearchTests
{
    [TestClass]
    
    public class FileStringHandlerTest
    {
        [TestMethod]
        public void StringsWithMaxSumTest(int x, int[,] y)
        {
            FileStringsHandler stringsHandler = new FileStringsHandler("../../../MyTextFile.txt");

            List<FileString> strings = stringsHandler.StringsWithMaxSum;

            IEnumerator<FileString> stringsEnumerator = strings.GetEnumerator();

            stringsEnumerator.MoveNext();

            Assert.AreEqual(string.Compare(stringsEnumerator.Current.FileStringLine, "540,127.1,0.11"),0);
        }

        [TestMethod]
        public void InvalidStringsTest()
        {
            FileStringsHandler stringsHandler = new FileStringsHandler("../../../MyTextFile.txt");

            List<FileString> strings = stringsHandler.InvalidStrings;

            IEnumerator<FileString> stringsEnumerator = strings.GetEnumerator();

            stringsEnumerator.MoveNext();

            Assert.AreEqual(string.Compare(stringsEnumerator.Current.FileStringLine, "5_.1,8.41,6.01"), 0);
        }
    }
}
