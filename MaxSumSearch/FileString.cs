﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;

namespace MaxSumSearch
{
    public class FileString
    {
        private static int fileLine = 1;

        public FileString(string fileString, string delim)
        {
            FileStringLine = fileString;
            FileLineNumber = fileLine++;
            IsStringCorrect = ValidateString();

            if (IsStringCorrect)
            {
                SubStrings = GetSubStrings(delim);
                StringSum = GetStringSum();
            }
        }

        public bool IsStringCorrect{ get; private set; }

        public double StringSum { get; private set; }

        public string FileStringLine { get; private set; }

        public int FileLineNumber { get; private set; }

        private string[] SubStrings { get; set; }

        private string[] GetSubStrings(string delimiter)
        {
            return FileStringLine.Split(delimiter);
        }

        private double GetStringSum()
        {
            var convertedNumbers = SubStrings.Select((item)=>double.Parse(item, CultureInfo.InvariantCulture)).ToList();
            return convertedNumbers.Sum();
        }

        private bool ValidateString()
        {
            var notNumbersTemplate = new Regex(@"[^-*\d|\.|,\s*]|\.{2,}|,{2,}|\.,|,\.|\.\d{1,}[^,]\.|0{2,}|-{2,}");

            return !notNumbersTemplate.IsMatch(FileStringLine) && !string.IsNullOrEmpty(FileStringLine);
        }
    }
}
