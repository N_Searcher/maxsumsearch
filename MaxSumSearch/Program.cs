﻿using System;
using System.IO;

namespace MaxSumSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintStrings(args);
        }

        static void PrintStrings(string[] filePaths)
        {
            try
            {
                var input = new UsersInput(filePaths);
                FileStringsOutput.ShowStrings(input.fileHandler.StringsWithMaxSum, "Correct strings:");

                FileStringsOutput.ShowStrings(input.fileHandler.InvalidStrings, "Invalid Strings: ");
                Console.Read();
            }
            catch (Exception error)
            {
                FileStringsOutput.ShowError(error);
            }
        }
    }
}