﻿using System;
using System.IO;

namespace MaxSumSearch
{
    public class UsersInput
    {
        private const string defaultMenuValue = "Default File";

        public FileStringsHandler fileHandler { get; set; }

        public UsersInput(string[] filePath)
        {
            var menu = new ConsoleMenu();

            var path = menu.ShowStartMenu() == defaultMenuValue && FileStringsHandler.ValidateFilePath(filePath[0]) ? filePath[0] : PathInsertion();

            fileHandler = new FileStringsHandler(path);
        }

        private string PathInsertion()
        {
            var isPathValid = false;
            string path = null;

            while (!isPathValid)
            {
                path = GetUsersInput();
                isPathValid = FileStringsHandler.ValidateFilePath(path);
            }

            return path;
        }

        private string GetUsersInput()
        {
            Console.Write("Insert path to file: ");
            return Console.ReadLine();
        }
    }
}
