﻿using System;
using System.Text;

namespace MaxSumSearch
{
    public class ConsoleMenu
    {
        private char selector = (char)9642;

        public string[] menuList = { "Select File", "Default File" };

        public string ShowStartMenu()
        {
            var key = ConsoleKey.NoName;
            Console.OutputEncoding = Encoding.UTF8;
            var row = 0;

            Console.Clear();

            OutputMenu(menuList);

            do
            {
                if (key == ConsoleKey.DownArrow && row + 1 < menuList.Length)
                {
                    SetSelectedMenuItem(row, ++row, menuList.Length);
                }
                else if (key == ConsoleKey.UpArrow && row - 1 >= 0)
                {
                    SetSelectedMenuItem(row, --row, menuList.Length);
                }
            } while ((key = Console.ReadKey().Key) != ConsoleKey.Enter);

            Console.Clear();

            return menuList[row];
        }

        private void OutputMenu(string[] menu)
        {
            for (int i = 0; i < menu.Length; i++)
            {
                Console.WriteLine("[" + (i == 0 ? selector.ToString() : " ") + "]" + menu[i]);
            }
        }

        private void SetSelectedMenuItem(int prevRow, int curRow, int cursorPos)
        {
            Console.SetCursorPosition(1, prevRow);
            Console.Write(" ");
            Console.SetCursorPosition(1, curRow);
            Console.Write(selector);
            Console.SetCursorPosition(0, cursorPos);
        }
    }
}
