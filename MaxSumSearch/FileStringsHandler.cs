﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System;

namespace MaxSumSearch
{
    public class FileStringsHandler
    {
        public FileStringsHandler(string filePath)
        {
            if (ValidateFilePath(filePath))
            {
                FileStrings = ReadFile(filePath);
                StringsWithMaxSum = GetStringsWithMaxSum();
                InvalidStrings = GetInvalidStrings();
            }
            else
            {
                FileStringsOutput.ShowError(new Exception("File does not exist or something went wrong!"));
            }
        }

        public List<FileString> StringsWithMaxSum { get; private set; }

        public List<FileString> InvalidStrings { get; private set; }

        private List<FileString> FileStrings { get; set; }

        public static bool ValidateFilePath(string filePath)
        {
            if (!File.Exists(filePath))
            {
                FileStringsOutput.ShowError(new Exception("Incorrect file path! Please re-insert the right one."));

                return false;
            }

            return true;
        }

        private List<FileString> GetStringsWithMaxSum()
        {
            var strings = FileStrings.Where(item => item.IsStringCorrect).ToList();
            var max = strings.Max(item => item.StringSum);
            return strings.Where(item => item.StringSum == max).ToList();
        }

        private List<FileString> GetInvalidStrings()
        {
            return FileStrings.Where(item => !item.IsStringCorrect).ToList();
        }

        private List<FileString> ReadFile(string path)
        {
            return File.ReadAllLines(path).Select(item => new FileString(item, ",")).ToList();
        }
    }
}
