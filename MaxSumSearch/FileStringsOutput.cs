﻿using System;
using System.Collections.Generic;

namespace MaxSumSearch
{
    static class FileStringsOutput
    {
        public static void ShowStrings(List<FileString> strings, string message)
        {
            Console.WriteLine(message);

            foreach (FileString item in strings)
            {
                Console.WriteLine(GetFormattedString(item));
            }

            Console.WriteLine();
        }

        public static void ShowError(Exception error)
        {
            Console.Clear();
            Console.WriteLine($"{error.Message}\r\n{error.TargetSite}");
        }

        private static string GetFormattedString(FileString fileString)
        {
            return "Line: " + fileString.FileLineNumber + "\tstring: " + fileString.FileStringLine + (fileString.IsStringCorrect ? "\tsum: " + fileString.StringSum : "");
        }
    }
}
